
public class Rectangle {
	int sideA;
	int sideB;
	
	public Rectangle() {
	System.out.println("Creating a rectangle");
	}
	
	public int area(){
		int result = sideA * sideB;
		return result;
	}
	
	public int perimeter(){
		return 2*sideA + 2*sideB;
	}

}
