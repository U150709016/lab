package midterm;
import java.util.ArrayList;


public abstract class Animal {
	
	private String name;
			
	public ArrayList<Animal> animals;
	
			
	public Animal(ArrayList<Animal> animals) {
		
		this.animals = animals;
	}
			
	public Animal() {
	}
			
	public Animal(Animal animal) {
				
		animals.add(animal);
				
	}
	
			
	public String getName() {
		return name;
	}
	
			
	public void setName(String name) {
		this.name = name;
	}
			
	public abstract String speak();
			
			
	public String toString() {
				
		return "" + animals.toString();
	}
}
		


